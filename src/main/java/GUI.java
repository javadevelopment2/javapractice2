import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

public class GUI {

    private static Cell[][] cells;

    public static void SaveCellsState()
    {
        JSONObject obj = new JSONObject();

        JSONArray cells = new JSONArray();
        JSONArray row = new JSONArray();

        for(int i=0;i<Constants.CELLS_COUNT_X;i++)
        {
            for(int j=0;j<Constants.CELLS_COUNT_Y;j++)
            {
                row.add(j, GUI.cells[i][j].getState());
            }
            cells.add(row);
        }

        obj.put("cells", cells);

        try {
            FileWriter file = new FileWriter("cellsState.json");
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void init(){
        initializeOpenGL();

        cells = new Cell[Constants.CELLS_COUNT_X][Constants.CELLS_COUNT_Y];

        Random rnd = new Random();

        for (int i = 0; i < Constants.CELLS_COUNT_X; i++) {
            for (int j = 0; j < Constants.CELLS_COUNT_Y; j++) {
                cells[i][j] = new Cell(i * Constants.CELL_SIZE, j * Constants.CELL_SIZE, (rnd.nextInt(100) < Constants.INITIAL_SPAWN_CHANCE ? -1 : 0));
            }
        }

    }

    private static void initializeOpenGL(){
        try {
            //Задаём размер будущего окна
            Display.setDisplayMode(new DisplayMode(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT));

            //Задаём имя будущего окна
            Display.setTitle(Constants.SCREEN_NAME);

            //Создаём окно
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Constants.SCREEN_WIDTH,0, Constants.SCREEN_HEIGHT,1,-1);
        glMatrixMode(GL_MODELVIEW);

        /*
         * Для поддержки текстур
         */
        glEnable(GL_TEXTURE_2D);

        /*
         * Для поддержки прозрачности
         */
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


        /*
         * Белый фоновый цвет
         */
        glClearColor(1,1,1,1);
    }
    public static void draw(){
        ///Очищает экран от старого изображения
        glClear(GL_COLOR_BUFFER_BIT);

        for(Cell[] line:cells){
            for(Cell cell:line){
                drawElement(cell);
            }
        }
    }
    private static void drawElement(Cell elem){
        ///Если у ячейки нет спрайта, то рисовать её не нужно
        if(elem.getSprite() == null) return;

        ///Собственно, рисуем. Подробно не останавливаюсь, так как нам интересна сама логика игры, а не LWJGL

        elem.getSprite().getTexture().bind();

        glBegin(GL_QUADS);
        glTexCoord2f(0,0);
        glVertex2f(elem.getX(),elem.getY()+elem.getHeight());
        glTexCoord2f(1,0);
        glVertex2f(elem.getX()+elem.getWidth(),elem.getY()+elem.getHeight());
        glTexCoord2f(1,1);
        glVertex2f(elem.getX()+elem.getWidth(), elem.getY());
        glTexCoord2f(0,1);
        glVertex2f(elem.getX(), elem.getY());
        glEnd();
    }
    public static void update(boolean have_to_decrease) {
        updateOpenGL();

        for(Cell[] line:cells){
            for(Cell cell:line){
                cell.update(have_to_decrease);
            }
        }
    }
    private static void updateOpenGL() {
        Display.update();
        Display.sync(Constants.FPS);
    }
    public enum Sprite {
        ///Файлы с именами circle и cherries должны лежать по адресу
        /// %папка проекта%/res/ в расширении .png
        BODY("circle"), CHERRIES("cherries");

        private Texture texture;

        Sprite(String texturename){
            try {
                this.texture = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/"+texturename+".png")));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public Texture getTexture(){
            return this.texture;
        }
    }

    public static int getState(int x, int y){
        return cells[x][y].getState();
    }

    public static void setState(int x,int y,int state){
        cells[x][y].setState(state);
    }

}
